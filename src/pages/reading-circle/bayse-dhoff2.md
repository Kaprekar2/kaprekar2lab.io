---
layout: /src/layouts/MarkdownPostLayout.astro
title: '標準ベイズ 2章'
pubDate: "2024-07"
description: ''
author: 'TAKARA Riki'
image:
    url: 'https://docs.astro.build/assets/full-logo-light.png'
    alt: 'Astroのロゴ。'
tags: ["数学"]
draft: true
---

<br />

## <span style="font-size: 1２0%;"> 概要 </span>
研究室での輪読会で選ばれたのでやります。<br />


## <span style="font-size: 150%;"> 信念と確率の公理 </span>

確率は**信念**（主観確率？的なものを指している）を表すのに用いられる.

**Be()**


## <span style="font-size: 150%;"> 1.2 なぜベイズか</span>
真の事前分布が不明でも近似値を与えて事後分布を推定可能


例では調査結果から（とりあえず）最も近似できてそうなパラメータを信念としてモデル作成者が置いている.

確度 $w$ は $a+b$ で表されるが、${w}\over{n+w}$サンプルサイズの大きさと同じ尺度から事前分布の信頼性を図っている.
 






<br />

$$





