---
layout: /src/layouts/MarkdownPostLayout.astro
title: '個人サイト作りました'
pubDate: "2023-10-25"
description: ''
author: 'TAKARA Riki'
image:
    url: 'https://docs.astro.build/assets/full-logo-light.png'
    alt: 'Astroのロゴ。'
tags: ["報告", "技術"]
draft: false
---

<br />

## <span style="font-size: 150%;"> 概要 </span>
個人サイトを作りました。<br />
自分自身について知ってもらえるような場所、輪読や独学を通して得た知識をまとめるwiki的な場所が欲しかったので勉強も兼ねて作成。<br />
<br />
HTMLとCSSをイチから書いて<u >[**Astro**](https://astro.build/)</u>というフレームワークを使いました。完全初心者でも、サクッとサイトの構築自体はできてとても使いやすいフレームワークな感じです。<br />
とはいえデプロイするまで一ヶ月以上かけて、ほぼ初めてHTML/CSSを書きながらググり力とコピペでイメージを実際に落とし込んでいく中で、BEM記法などまで気を使えず煩雑なコードになったりビルドエラーに対処できなかったりと産みの苦しみを味わったのも事実。<br />
とりあえず形はできたのでまだまだ整備と更新が必要なので根気入れて作業を続けていきたいなと思ってます。<br />
所属の更新とかもこちらに記載してあるので、よければご参考まで。<br />
<br />
あとこれは単純な感想なんですが、HTML/CSS自体は大学で履修したサンスクリット語よりは簡単である可能性に気づいて同時に自信もつけられた気がする。<br />
みんなもサンスクリット語をやろう！教科書は<u>[これ](https://www.amazon.co.jp/%E3%82%B5%E3%83%B3%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%83%E3%83%88%E8%AA%9E%E5%88%9D%E7%AD%89%E6%96%87%E6%B3%95%E2%80%95%E7%B7%B4%E7%BF%92%E9%A1%8C-%E9%81%B8%E6%96%87-%E8%AA%9E%E5%BD%99%E4%BB%98-J-%E3%82%B4%E3%83%B3%E3%83%80/dp/4393101081/ref=sr_1_3?__mk_ja_JP=%E3%82%AB%E3%82%BF%E3%82%AB%E3%83%8A&crid=25NFFUOTMX7WG&keywords=%E3%82%B5%E3%83%B3%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%83%E3%83%88%E8%AA%9E&qid=1698219188&sprefix=%E3%82%B5%E3%83%B3%E3%82%B9%E3%82%AF%E3%83%AA%E3%83%83%E3%83%88%E8%AA%9E%2Caps%2C177&sr=8-3)</u>です。

